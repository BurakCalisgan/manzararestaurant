﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ManzaraRestaurant
{
    class cKullanicilar
    {
        cGenel genel = new cGenel();

        #region Property

        public string P_TYPE { get; set; }
        public bool _KullaniciDurum { get; set; }
        public int _KullaniciId { get; set; }
        public string _KullaniciAd { get; set; }
        public string _KullaniciSoyad { get; set; }
        public string _KullaniciParola { get; set; }
        public string _KullaniciAdi { get; set; }
        public string _KullaniciTip { get; set; }

        #endregion

        #region KullaniciEkle
        public void KullaniciEkle(cKullanicilar cK)
        {
            SqlConnection con = new SqlConnection(genel.connectionString);
            SqlCommand cmd = new SqlCommand("Kullanicilar_Insert", con);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                cmd.Parameters.AddWithValue("@P_AD", cK._KullaniciAd);
                cmd.Parameters.AddWithValue("@P_SOYAD", cK._KullaniciSoyad);
                cmd.Parameters.AddWithValue("@P_PAROLA", cK._KullaniciParola);
                cmd.Parameters.AddWithValue("@P_KULLANICI_ADI", cK._KullaniciAdi);
                cmd.Parameters.AddWithValue("@P_KULLANICI_TIP", cK._KullaniciTip);
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();

                }
                cmd.ExecuteNonQuery();
                con.Close();
                MessageBox.Show("Kullanıcı Ekleme İşleminiz Başarılı Bir Şekilde Gerçekleştirilmiştir.", "Bilgi", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                string hata = ex.Message;
                throw;
            }

        }
        #endregion

        #region KullaniciGuncelle
        public void KullaniciGuncelle(cKullanicilar cK)
        {
            SqlConnection con = new SqlConnection(genel.connectionString);
            SqlCommand cmd = new SqlCommand("Kullanicilar_Update", con);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                cmd.Parameters.AddWithValue("@P_ID", cK._KullaniciId);
                cmd.Parameters.AddWithValue("@P_AD", cK._KullaniciAd);
                cmd.Parameters.AddWithValue("@P_SOYAD", cK._KullaniciSoyad);
                cmd.Parameters.AddWithValue("@P_PAROLA", cK._KullaniciParola);
                cmd.Parameters.AddWithValue("@P_KULLANICI_ADI", cK._KullaniciAdi);
                cmd.Parameters.AddWithValue("@P_KULLANICI_TIP", cK._KullaniciTip);
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();

                }
                cmd.ExecuteNonQuery();
                con.Close();
                MessageBox.Show("Kullanıcı Güncelleme İşleminiz Başarılı Bir Şekilde Gerçekleştirilmiştir.", "Bilgi", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                string hata = ex.Message;
                throw;
            }
        }
        #endregion

        #region KullaniciSil
        public void KullaniciSil(cKullanicilar cK)
        {
            SqlConnection con = new SqlConnection(genel.connectionString);
            SqlCommand cmd = new SqlCommand("Kullanicilar_Delete", con);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                cmd.Parameters.AddWithValue("@P_ID", cK._KullaniciId);
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();

                }
                cmd.ExecuteNonQuery();
                con.Close();
                MessageBox.Show("Kullanıcı Silme İşleminiz Başarılı Bir Şekilde Gerçekleştirilmiştir.", "Bilgi", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                string hata = ex.Message;
                throw;
            }
        }
        #endregion

        #region DML_Kullanicilar
        public void DMLKullanicilar(cKullanicilar cK)
        {
            string islemTipi = cK.P_TYPE;
            SqlConnection con = new SqlConnection(genel.connectionString);
            SqlCommand cmd = new SqlCommand("DML_Kullanicilar", con);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                if (islemTipi == "I")
                {
                    cmd.Parameters.AddWithValue("@P_TYPE", cK.P_TYPE);
                    cmd.Parameters.AddWithValue("@P_AD", cK._KullaniciAd);
                    cmd.Parameters.AddWithValue("@P_SOYAD", cK._KullaniciSoyad);
                    cmd.Parameters.AddWithValue("@P_PAROLA", cK._KullaniciParola);
                    cmd.Parameters.AddWithValue("@P_KULLANICI_ADI", cK._KullaniciAdi);
                    cmd.Parameters.AddWithValue("@P_KULLANICI_TIP", cK._KullaniciTip);
                    if (con.State == ConnectionState.Closed)
                    {
                        con.Open();

                    }
                    cmd.ExecuteNonQuery();
                    con.Close();
                    MessageBox.Show("Kullanıcı Ekleme İşleminiz Başarılı Bir Şekilde Gerçekleştirilmiştir.","Bilgi",MessageBoxButtons.OK,MessageBoxIcon.Information);
                }
                else if(islemTipi == "U")
                {
                    cmd.Parameters.AddWithValue("@P_TYPE", cK.P_TYPE);
                    cmd.Parameters.AddWithValue("@P_ID", cK._KullaniciId);
                    cmd.Parameters.AddWithValue("@P_AD", cK._KullaniciAd);
                    cmd.Parameters.AddWithValue("@P_SOYAD", cK._KullaniciSoyad);
                    cmd.Parameters.AddWithValue("@P_PAROLA", cK._KullaniciParola);
                    cmd.Parameters.AddWithValue("@P_KULLANICI_ADI", cK._KullaniciAdi);
                    cmd.Parameters.AddWithValue("@P_KULLANICI_TIP", cK._KullaniciTip);
                }
                else if(islemTipi == "U")
                {
                    cmd.Parameters.AddWithValue("@P_TYPE", cK.P_TYPE);
                    cmd.Parameters.AddWithValue("@P_ID", cK._KullaniciId);
                }
                else
                {
                    MessageBox.Show("Beklenmedik Bir İşlem Tipi");
                }
            }
            catch (Exception ex)
            {
                string hata = ex.Message;
                throw;
            }
        }
        #endregion

        #region PersonelEntryControl
        public bool PersonelEntryControl(string password, int UserId)
        {
            bool result = false;
            SqlConnection con = new SqlConnection(genel.connectionString);
            SqlCommand cmd = new SqlCommand("KullaniciGiris", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@P_PASSWORD", password);
            cmd.Parameters.AddWithValue("@P_ID", UserId);
            try
            {
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();

                }
                result = Convert.ToBoolean(cmd.ExecuteScalar());

            }
            catch (SqlException ex)
            {
                string hata = ex.Message;

                throw;
            }
            return result;
        }
        #endregion

        #region GetKullanicilarInfo
        public void GetKullanicilarInfo(ComboBox cb)
        {
            cb.Items.Clear();
            SqlConnection con = new SqlConnection(genel.connectionString);
            SqlCommand cmd = new SqlCommand("GetKullanicilarInfo", con);
            cmd.CommandType = CommandType.StoredProcedure;
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
            }
            SqlDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                cKullanicilar k = new cKullanicilar();
                k._KullaniciId = Convert.ToInt32(dr["ID"]);
                k._KullaniciAd = Convert.ToString(dr["AD"]);
                k._KullaniciSoyad = Convert.ToString(dr["SOYAD"]);
                k._KullaniciParola = Convert.ToString(dr["PAROLA"]);
                k._KullaniciAdi = Convert.ToString(dr["KULLANICIADI"]);
                k._KullaniciTip = Convert.ToString(dr["KULLANICITIP"]);
                k._KullaniciDurum = Convert.ToBoolean(dr["DURUM"]);

                cb.Items.Add(k);
            }
            dr.Close();
            con.Close();
        }
        #endregion

        #region GetKullanicilarForYonetici
        public DataTable GetKullanicilarForYonetici()
        {
            SqlConnection con = new SqlConnection(genel.connectionString);
            SqlDataAdapter adapter = new SqlDataAdapter("GetKullanicilarForYonetici", con);
            DataTable dt = new DataTable();
            try
            {
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }

                adapter.Fill(dt);


            }
            catch (SqlException ex)
            {
                string hata = ex.Message;

                throw;
            }

            return dt;
        }

        #endregion

        #region GetKullaniciTip
        public string GetKullaniciTip(int kullaniciId)
        {
            string kTip;
            SqlConnection con = new SqlConnection(genel.connectionString);
            SqlCommand cmd = new SqlCommand("GetKullaniciTip", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@P_KullaniciId", kullaniciId);
            try
            {
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();

                }
                kTip = Convert.ToString(cmd.ExecuteScalar());

            }
            catch (SqlException ex)
            {
                string hata = ex.Message;

                throw;
            }

            return kTip;
        }
        #endregion

        public override string ToString()
        {
            return _KullaniciAd + " " + _KullaniciSoyad;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ManzaraRestaurant
{
    public partial class frmKullaniciEkle : Form
    {
        cKullanicilar cK = new cKullanicilar();
        int ID = 0;
        public frmKullaniciEkle()
        {
            InitializeComponent();
            cKullanicilar cK = new cKullanicilar();
            dtKullanicilar.DataSource = cK.GetKullanicilarForYonetici();
        }

        #region ClearData
        public void ClearData()
        {
            ID = 0;
            txtAd.Text = "";
            txtSoyad.Text = "";
            txtSifre.Text = "";
            txtKullaniciAdi.Text = "";
            txtKullaniciTip.Text = "";
        }
        #endregion

        #region btnEkle_Click
        private void btnEkle_Click(object sender, EventArgs e)
        {
            if ((txtAd.Text != null && txtAd.Text != "") && (txtSoyad.Text != null && txtSoyad.Text != "") && (txtSifre.Text != null && txtSifre.Text != "") && (txtKullaniciAdi.Text != null && txtKullaniciAdi.Text != "") && (txtKullaniciTip.Text != null && txtKullaniciTip.Text != ""))
            {
                cK._KullaniciAd = txtAd.Text;
                cK._KullaniciSoyad = txtSoyad.Text;
                cK._KullaniciParola = txtSifre.Text;
                cK._KullaniciAdi = txtKullaniciAdi.Text;
                cK._KullaniciTip = txtKullaniciTip.Text;
                cK.KullaniciEkle(cK);
                ClearData();
                dtKullanicilar.DataSource = cK.GetKullanicilarForYonetici();
            }
            else
            {
                MessageBox.Show("Bilgileri Eksiksiz Doldurmalısınız !", "Bilgi", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        #endregion

        #region dtKullanicilar_RowHeaderMouseClick
        private void dtKullanicilar_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            ID = Convert.ToInt32(dtKullanicilar.Rows[e.RowIndex].Cells[0].Value.ToString());
            txtAd.Text = dtKullanicilar.Rows[e.RowIndex].Cells[1].Value.ToString();
            txtSoyad.Text = dtKullanicilar.Rows[e.RowIndex].Cells[2].Value.ToString();
            txtSifre.Text = dtKullanicilar.Rows[e.RowIndex].Cells[3].Value.ToString();
            txtKullaniciAdi.Text = dtKullanicilar.Rows[e.RowIndex].Cells[4].Value.ToString();
            txtKullaniciTip.Text = dtKullanicilar.Rows[e.RowIndex].Cells[5].Value.ToString();
        }
        #endregion

        #region btnGuncelle_Click
        private void btnGuncelle_Click(object sender, EventArgs e)
        {
            if ((txtAd.Text != null && txtAd.Text != "") && (txtSoyad.Text != null && txtSoyad.Text != "") && (txtSifre.Text != null && txtSifre.Text != "") && (txtKullaniciAdi.Text != null && txtKullaniciAdi.Text != "") && (txtKullaniciTip.Text != null && txtKullaniciTip.Text != ""))
            {
                cK._KullaniciId = ID;
                cK._KullaniciAd = txtAd.Text;
                cK._KullaniciSoyad = txtSoyad.Text;
                cK._KullaniciParola = txtSifre.Text;
                cK._KullaniciAdi = txtKullaniciAdi.Text;
                cK._KullaniciTip = txtKullaniciTip.Text;
                cK.KullaniciGuncelle(cK);
                ClearData();
                dtKullanicilar.DataSource = cK.GetKullanicilarForYonetici();
            }
            else
            {
                MessageBox.Show("Bilgileri Eksiksiz Doldurmalısınız !", "Bilgi", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        #endregion

        #region btnSil_Click
        private void btnSil_Click(object sender, EventArgs e)
        {
            if (ID != 0)
            {
                cK._KullaniciId = ID;
                cK.KullaniciSil(cK);
                ClearData();
                dtKullanicilar.DataSource = cK.GetKullanicilarForYonetici();
            }
            else
            {
                MessageBox.Show("Lütfen Silmek İçin Bir Satır Seçiniz !", "Bilgi", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        #endregion
    }
}

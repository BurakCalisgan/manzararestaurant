﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ManzaraRestaurant
{
    public partial class frmMasalar : Form
    {

        public frmMasalar()
        {
            InitializeComponent();
        }

        #region Buton Click Olayları
        
        private void btnGeriDon_Click(object sender, EventArgs e)
        {
            frmMenu menu = new frmMenu();
            this.Close();
            menu.Show();
        }

        private void btnCikis_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Çıkmak İstediğinizden Emin Misiniz?", "Uyarı !", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        private void btnMasa1_Click(object sender, EventArgs e)
        {
            frmSiparis siparis = new frmSiparis();
            string masa1 = "MASA 1";
            int uzunluk = masa1.Length;
            int uzunluk2 = btnMasa1.Name.Length;

            cGenel._buttonValue = masa1.Substring(uzunluk - 6, 6);
            cGenel._buttonName = btnMasa1.Name.Substring(uzunluk2 - 8, 8);
            this.Close();
            siparis.ShowDialog();

        }

        private void btnMasa2_Click(object sender, EventArgs e)
        {
            frmSiparis siparis = new frmSiparis();
            string masa2 = "MASA 2";
            int uzunluk = masa2.Length;
            int uzunluk2 = btnMasa2.Name.Length;

            cGenel._buttonValue = masa2.Substring(uzunluk - 6, 6);
            cGenel._buttonName = btnMasa2.Name.Substring(uzunluk2 - 8, 8);
            this.Close();
            siparis.ShowDialog();
        }

        private void btnMasa3_Click(object sender, EventArgs e)
        {
            frmSiparis siparis = new frmSiparis();
            string masa3 = "MASA 3";
            int uzunluk = masa3.Length;
            int uzunluk2 = btnMasa3.Name.Length;

            cGenel._buttonValue = masa3.Substring(uzunluk - 6, 6);
            cGenel._buttonName = btnMasa3.Name.Substring(uzunluk2 - 8, 8);
            this.Close();
            siparis.ShowDialog();
        }

        private void btnMasa4_Click(object sender, EventArgs e)
        {
            frmSiparis siparis = new frmSiparis();
            string masa4 = "MASA 4";
            int uzunluk = masa4.Length;
            int uzunluk2 = btnMasa4.Name.Length;

            cGenel._buttonValue = masa4.Substring(uzunluk - 6, 6);
            cGenel._buttonName = btnMasa4.Name.Substring(uzunluk2 - 8, 8);
            this.Close();
            siparis.ShowDialog();
        }

        private void btnMasa5_Click(object sender, EventArgs e)
        {
            frmSiparis siparis = new frmSiparis();
            string masa5 = "MASA 5";
            int uzunluk = masa5.Length;
            int uzunluk2 = btnMasa5.Name.Length;

            cGenel._buttonValue = masa5.Substring(uzunluk - 6, 6);
            cGenel._buttonName = btnMasa5.Name.Substring(uzunluk2 - 8, 8);
            this.Close();
            siparis.ShowDialog();
        }
        #endregion

        cGenel genel = new cGenel();

        #region frmMasalar_Load
        private void frmMasalar_Load(object sender, EventArgs e)
        {
            SqlConnection con = new SqlConnection(genel.connectionString);
            SqlCommand cmd = new SqlCommand("GetMasalarDurum", con);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataReader dr = null;
            if (con.State == ConnectionState.Closed)
            {
                con.Open();

            }
            dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                foreach (Control item in this.Controls)
                {
                    if (item is Button)
                    {
                        if (item.Name == "btnMasa" + dr["ID"].ToString() && dr["DURUM"].ToString() == "1")
                        {
                            item.BackColor = Color.Green;
                        }
                        else if (item.Name == "btnMasa" + dr["ID"].ToString() && dr["DURUM"].ToString() == "2")
                        {
                            cMasalar masalar = new cMasalar();
                            DateTime dTime = Convert.ToDateTime(masalar.SessionSum(2, dr["ID"].ToString()));
                            DateTime dTime2 = DateTime.Now;

                            string st1 = Convert.ToDateTime(masalar.SessionSum(2, dr["ID"].ToString())).ToShortDateString();
                            string st2 = DateTime.Now.ToShortDateString();

                            DateTime time = dTime.AddMinutes(DateTime.Parse(st1).TimeOfDay.TotalMinutes);
                            DateTime time2 = dTime2.AddMinutes(DateTime.Parse(st2).TimeOfDay.TotalMinutes);

                            var fark = time2 - time;

                            item.Text = String.Format("{0}{1}{2}",
                                fark.Days > 0 ? string.Format("{0} Gün ", fark.Days) : "",
                                fark.Hours > 0 ? string.Format("{0} Saat ", fark.Hours) : "",
                                fark.Minutes > 0 ? string.Format("{0} Dakika", fark.Minutes) : "").Trim() + "\n\n\nMASA " + dr["ID"].ToString();

                            item.BackColor = Color.Red;
                        }
                        else if (item.Name == "btnMasa" + dr["ID"].ToString() && dr["DURUM"].ToString() == "3")
                        {
                            item.BackColor = Color.Aqua;
                        }
                        else if (item.Name == "btnMasa" + dr["ID"].ToString() && dr["DURUM"].ToString() == "4")
                        {
                            item.BackColor = Color.IndianRed;
                        }
                    }
                }

            }
        }
        #endregion

    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ManzaraRestaurant
{
    public partial class frmGiris : Form
    {

        public frmGiris()
        {
            InitializeComponent();
        }


        #region frmGiris_Load
        private void frmGiris_Load(object sender, EventArgs e)
        {

            cKullanicilar k = new cKullanicilar();
            k.GetKullanicilarInfo(cbKullanici);

        }
        #endregion

        #region btnGiris_Click
        private void btnGiris_Click(object sender, EventArgs e)
        {
            cGenel genel = new cGenel();
            cKullanicilar p = new cKullanicilar();
            bool result = p.PersonelEntryControl(txtSifre.Text, cGenel._kullaniciId);
            string kullaniciTipi;
            if (result)
            {
                kullaniciTipi = p.GetKullaniciTip(cGenel._kullaniciId);
                if (kullaniciTipi == "P")
                {
                    cPersonelHareketleri cPH = new cPersonelHareketleri();
                    cPH._PersonelId = cGenel._personelId;
                    cPH.Islem = "Giriş Yaptı";
                    cPH._Tarih = DateTime.Now;
                    cPH.PersonelActionSave(cPH);

                    this.Hide();
                    frmMenu menu = new frmMenu();
                    menu.Show();
                }
                else
                {
                    this.Hide();
                    frmYoneticiPanel yoneticiPanel = new frmYoneticiPanel();
                    yoneticiPanel.Show();
                }
            }
            else
            {
                MessageBox.Show("Hatalı Kullanıcı Adı veya Şifre","",MessageBoxButtons.OK,MessageBoxIcon.Error);
            }
        }
#endregion

        #region cbKullanici_SelectedIndexChanged
        private void cbKullanici_SelectedIndexChanged(object sender, EventArgs e)
        {
            cKullanicilar p = new cKullanicilar();
            cKullanicilar k = (cKullanicilar)cbKullanici.SelectedItem;
            
            string kullaniciTipi;
            kullaniciTipi = p.GetKullaniciTip(k._KullaniciId);
            if (kullaniciTipi == "P")
            {
                cGenel._personelId = k._KullaniciId;
                cGenel._kullaniciId = k._KullaniciId;
            }
            else
            {
                cGenel._kullaniciId = k._KullaniciId;
            }

        }
        #endregion

        #region btnCikis_Click
        private void btnCikis_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Uygulamayı Kapatmak İstediğinize Emin Misiniz ?", "Uyarı !", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                Application.Exit();
            }
        }
        #endregion
    }
}

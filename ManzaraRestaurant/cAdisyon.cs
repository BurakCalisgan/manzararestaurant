﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManzaraRestaurant
{
    class cAdisyon
    {
        cGenel genel = new cGenel();
        #region Property
        public int ID { get; set; }
        public int _ServisTurNo { get; set; }
        public decimal _Tutar { get; set; }
        public DateTime _Tarih { get; set; }
        public int _PersonelId { get; set; }
        public int _MasaId { get; set; }
        public int _Durum { get; set; }
        #endregion

        #region GetByAddition
        public int GetByAddition(int masaId)
        {
            SqlConnection con = new SqlConnection(genel.connectionString);
            SqlCommand cmd = new SqlCommand("GetByAddition", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@P_MASA_ID", masaId);

            try
            {
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }
                masaId = Convert.ToInt32(cmd.ExecuteScalar());

            }
            catch (SqlException ex)
            {
                string hata = ex.Message;
                throw;
            }
            finally
            {
                con.Dispose();
                con.Close();

            }
            return masaId;
        }
        #endregion

        #region SetByAdditionNew
        public bool SetByAdditionNew(cAdisyon Bilgiler)
        {
            bool result = false;
            SqlConnection con = new SqlConnection(genel.connectionString);
            SqlCommand cmd = new SqlCommand("SetByAdditionNew", con);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }
                cmd.Parameters.AddWithValue("@P_SERVIS_TUR_NO", Bilgiler._ServisTurNo);
                cmd.Parameters.AddWithValue("@P_TARIH", Bilgiler._Tarih);
                cmd.Parameters.AddWithValue("@P_PERSONEL_ID", Bilgiler._PersonelId);
                cmd.Parameters.AddWithValue("@P_MASA_ID", Bilgiler._MasaId);
                cmd.Parameters.AddWithValue("@P_DURUM", Bilgiler._Durum);
                result = Convert.ToBoolean(cmd.ExecuteNonQuery());
            }
            catch (SqlException ex)
            {
                string hata = ex.Message;
                throw;
            }
            finally
            {
                con.Dispose();
                con.Close();

            }
            return result;
        }
        #endregion

        #region AdditionClose
        //Hesap kapatılınca rezervasyonuda kapat 
        public void AdditionClose(int adisyonId, int durum)
        {
            SqlConnection con = new SqlConnection(genel.connectionString);
            SqlCommand cmd = new SqlCommand("AdditionClose", con);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }

                cmd.Parameters.AddWithValue("@P_ADISYON_ID", adisyonId);
                cmd.Parameters.AddWithValue("@P_DURUM", durum);

                cmd.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                string hata = ex.Message;
                throw;
            }
            finally
            {
                con.Dispose();
                con.Close();
            }
        }
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ManzaraRestaurant
{
    public partial class frmYoneticiPanel : Form
    {
        public frmYoneticiPanel()
        {
            InitializeComponent();
        }

        #region btnKullaniciEkle_Click
        private void btnKullaniciEkle_Click(object sender, EventArgs e)
        {
            frmKullaniciEkle frmKEkle = new frmKullaniciEkle();
            frmKEkle.Show();
        }
        #endregion

        #region btnPersonelHareketleri_Click
        private void btnPersonelHareketleri_Click(object sender, EventArgs e)
        {
            frmPersonelHareketleri frmPHareketleri = new frmPersonelHareketleri();
            frmPHareketleri.Show();
        }
        #endregion

        #region btnCikis_Click
        private void btnCikis_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Uygulamayı Kapatmak İstediğinize Emin Misiniz ?", "Uyarı !", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                Application.Exit();
            }
        }
        #endregion

    }
}

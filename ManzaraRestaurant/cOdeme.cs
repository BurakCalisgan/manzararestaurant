﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManzaraRestaurant
{
    class cOdeme
    {
        cGenel genel = new cGenel();
        #region Property
        public int _ID { get; set; }
        public int _AdisyonId { get; set; }
        public int _OdemeTurId { get; set; }
        public decimal _AraToplam { get; set; }
        public decimal _Indirim { get; set; }
        public decimal _KdvTutari{ get; set; }
        public decimal _GenelToplam { get; set; }
        public DateTime _Tarih { get; set; }
        public int _MusteriId { get; set; }
        #endregion

        #region BillClose
        //Müşterinin masasını kapatma
        public bool BillClose(cOdeme hesap)
        {
            bool result = false;
            SqlConnection con = new SqlConnection(genel.connectionString);
            SqlCommand cmd = new SqlCommand("BillClose", con);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }
                cmd.Parameters.AddWithValue("@P_ADISYONID", hesap._AdisyonId);
                cmd.Parameters.AddWithValue("@P_ODEMETURID", hesap._OdemeTurId);
                cmd.Parameters.AddWithValue("@P_MUSTERIID", hesap._MusteriId);
                cmd.Parameters.AddWithValue("@P_ARATOPLAM", hesap._AraToplam);
                cmd.Parameters.AddWithValue("@P_KDVTUTARI", hesap._KdvTutari);
                cmd.Parameters.AddWithValue("@P_INDIRIM", hesap._Indirim);
                cmd.Parameters.AddWithValue("@P_GENELTOPLAM", hesap._GenelToplam);
                result = Convert.ToBoolean(cmd.ExecuteNonQuery());
            }
            catch (SqlException ex)
            {
                string hata = ex.Message;
                throw;
            }
            finally
            {
                con.Dispose();
                con.Close();
            }
            return result;
        }
        #endregion

        #region SumTotalForClientId
        //Müşterinin toplam hesabını alıyoruz.
        public decimal SumTotalForClientId(int musteriId)
        {
            decimal total = 0;
            SqlConnection con = new SqlConnection(genel.connectionString);
            SqlCommand cmd = new SqlCommand("SumTotalForClientId", con);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }

                cmd.Parameters.AddWithValue("@P_MUSTERI_ID ",musteriId);

                total = Convert.ToDecimal(cmd.ExecuteScalar());
            }
            catch (SqlException ex)
            {
                string hata = ex.Message;
                throw;
            }
            finally
            {
                con.Dispose();
                con.Close();
            }
            
            return total;
        }
        #endregion
    }
}

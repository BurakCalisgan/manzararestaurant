﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ManzaraRestaurant
{
    public partial class frmSiparis : Form
    {
        public frmSiparis()
        {
            InitializeComponent();
        }

        #region Çıkış - Geri Dön Butonları


        private void btnCikis_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Çıkmak İstediğinizden Emin Misiniz?", "Uyarı !", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        private void btnGeriDon_Click(object sender, EventArgs e)
        {
            frmMenu menu = new frmMenu();
            this.Close();
            menu.Show();
        }
        #endregion

        #region Hesap Makinesi Gibi Kullandığımız Kısım
        void Islem(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            switch (btn.Name)
            {
                case "btn1":
                    txtAdet.Text += (1).ToString();
                    break;
                case "btn2":
                    txtAdet.Text += (2).ToString();
                    break;
                case "btn3":
                    txtAdet.Text += (3).ToString();
                    break;
                case "btn4":
                    txtAdet.Text += (4).ToString();
                    break;
                case "btn5":
                    txtAdet.Text += (5).ToString();
                    break;
                case "btn6":
                    txtAdet.Text += (6).ToString();
                    break;
                case "btn7":
                    txtAdet.Text += (7).ToString();
                    break;
                case "btn8":
                    txtAdet.Text += (8).ToString();
                    break;
                case "btn9":
                    txtAdet.Text += (9).ToString();
                    break;
                case "btn0":
                    txtAdet.Text += (0).ToString();
                    break;
                case "btnC":
                    txtAdet.Text = "";
                    break;

                default:
                    MessageBox.Show("Sayı Girmelisiniz !");
                    break;
            }
        }
        #endregion

        int tableId = 0; int additionId = 0;
        #region frmSiparis_Load
        private void frmSiparis_Load(object sender, EventArgs e)
        {
            lblMasaNo.Text = cGenel._buttonValue;

            cMasalar masa = new cMasalar();
            tableId = masa.TableGetbyNumber(cGenel._buttonName);

            if (masa.TableGetbyState(tableId, 2) == true || masa.TableGetbyState(tableId, 4) == true)
            {
                cAdisyon cA = new cAdisyon();
                additionId = cA.GetByAddition(tableId);
                cSiparisler cS = new cSiparisler();
                cS.GetByOrder(lvSiparisler, additionId);
            }

            btn1.Click += new EventHandler(Islem);
            btn2.Click += new EventHandler(Islem);
            btn3.Click += new EventHandler(Islem);
            btn4.Click += new EventHandler(Islem);
            btn5.Click += new EventHandler(Islem);
            btn6.Click += new EventHandler(Islem);
            btn7.Click += new EventHandler(Islem);
            btn8.Click += new EventHandler(Islem);
            btn9.Click += new EventHandler(Islem);
            btn0.Click += new EventHandler(Islem);
            btnC.Click += new EventHandler(Islem);

        }
        #endregion

        cUrunCesitleri cUC = new cUrunCesitleri();
        #region ButtonClick Olayları


        private void btnAnaYemek1_Click(object sender, EventArgs e)
        {

            cUC.GetByProductTypes(lvMenu, btnAnaYemek1);
        }

        private void btnİcecek2_Click(object sender, EventArgs e)
        {
            cUC.GetByProductTypes(lvMenu, btnİcecek2);
        }

        private void btnTatlilar3_Click(object sender, EventArgs e)
        {
            cUC.GetByProductTypes(lvMenu, btnTatlilar3);
        }

        private void btnSalata4_Click(object sender, EventArgs e)
        {
            cUC.GetByProductTypes(lvMenu, btnSalata4);
        }

        private void btnFastFood5_Click(object sender, EventArgs e)
        {
            cUC.GetByProductTypes(lvMenu, btnFastFood5);
        }

        private void btnCorba6_Click(object sender, EventArgs e)
        {
            cUC.GetByProductTypes(lvMenu, btnCorba6);
        }

        private void btnMakarna7_Click(object sender, EventArgs e)
        {
            cUC.GetByProductTypes(lvMenu, btnMakarna7);
        }

        private void btnAraSicak8_Click(object sender, EventArgs e)
        {
            cUC.GetByProductTypes(lvMenu, btnAraSicak8);
        }

        private void lvMenu_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            int sayac = 0, sayac2 = 0;
            if (txtAdet.Text == "")
            {
                txtAdet.Text = "1";
            }

            if (lvMenu.Items.Count > 0)
            {
                sayac = lvSiparisler.Items.Count;
                lvSiparisler.Items.Add(lvMenu.SelectedItems[0].Text);
                lvSiparisler.Items[sayac].SubItems.Add(txtAdet.Text);
                lvSiparisler.Items[sayac].SubItems.Add(lvMenu.SelectedItems[0].SubItems[2].Text);
                lvSiparisler.Items[sayac].SubItems.Add((Convert.ToDecimal(lvMenu.SelectedItems[0].SubItems[1].Text) * Convert.ToDecimal(txtAdet.Text)).ToString());
                lvSiparisler.Items[sayac].SubItems.Add("0");
                sayac2 = lvYeniEklenenler.Items.Count;
                lvSiparisler.Items[sayac].SubItems.Add(sayac2.ToString());


                lvYeniEklenenler.Items.Add(additionId.ToString());
                lvYeniEklenenler.Items[sayac2].SubItems.Add(lvMenu.SelectedItems[0].SubItems[2].Text);
                lvYeniEklenenler.Items[sayac2].SubItems.Add(txtAdet.Text);
                lvYeniEklenenler.Items[sayac2].SubItems.Add(tableId.ToString());
                lvYeniEklenenler.Items[sayac2].SubItems.Add(sayac2.ToString());

                sayac2++;

                txtAdet.Text = "";
            }

        }

        ArrayList silinenler = new ArrayList();
        private void btnSiparis_Click(object sender, EventArgs e)
        {
            /*
             * 1- Masa Boş
             * 2- Masa Dolu
             * 3- Masa Rezerve
             * 4- Dolu Rezerve
             */

            cMasalar masa = new cMasalar();
            cAdisyon newAdisyon = new cAdisyon();
            cSiparisler saveOrder = new cSiparisler();
            frmMasalar frmMasa = new frmMasalar();
            bool result = false;
            if (masa.TableGetbyState(tableId, 1) == true)
            {
                newAdisyon._ServisTurNo = 1;
                newAdisyon._PersonelId = cGenel._personelId;
                newAdisyon._MasaId = tableId;
                newAdisyon._Tarih = DateTime.Now;
                newAdisyon._Durum = 0;
                result = newAdisyon.SetByAdditionNew(newAdisyon);
                masa.SetUpdateTableState(cGenel._buttonName, 2);

                if (lvSiparisler.Items.Count > 0)
                {
                    for (int i = 0; i < lvSiparisler.Items.Count; i++)
                    {
                        saveOrder._MasaId = tableId;
                        saveOrder._UrunId = Convert.ToInt32(lvSiparisler.Items[i].SubItems[2].Text);
                        saveOrder._AdisyonId = newAdisyon.GetByAddition(tableId);
                        saveOrder._Adet = Convert.ToInt32(lvSiparisler.Items[i].SubItems[1].Text);
                        saveOrder.SetSaveOrder(saveOrder);
                    }
                    this.Close();
                    frmMasa.Show();
                }
            }
            else if (masa.TableGetbyState(tableId, 2) == true || masa.TableGetbyState(tableId, 4) == true)
            {
                if (lvYeniEklenenler.Items.Count > 0)
                {
                    for (int i = 0; i < lvYeniEklenenler.Items.Count; i++)
                    {
                        saveOrder._MasaId = tableId;
                        saveOrder._UrunId = Convert.ToInt32(lvYeniEklenenler.Items[i].SubItems[1].Text);
                        saveOrder._AdisyonId = newAdisyon.GetByAddition(tableId);
                        saveOrder._Adet = Convert.ToInt32(lvYeniEklenenler.Items[i].SubItems[2].Text);
                        saveOrder.SetSaveOrder(saveOrder);
                    }
                }
                if (silinenler.Count > 0)
                {
                    foreach (string item in silinenler)
                    {
                        saveOrder.SetDeleteOrder(Convert.ToInt32(item));
                    }
                }

                this.Close();
                frmMasa.Show();
            }
            else if (masa.TableGetbyState(tableId, 3) == true)
            {
                newAdisyon._ServisTurNo = 1;
                newAdisyon._PersonelId = cGenel._personelId;
                newAdisyon._MasaId = tableId;
                newAdisyon._Tarih = DateTime.Now;
                newAdisyon._Durum = 0;
                result = newAdisyon.SetByAdditionNew(newAdisyon);
                masa.SetUpdateTableState(cGenel._buttonName, 4);

                if (lvSiparisler.Items.Count > 0)
                {
                    for (int i = 0; i < lvSiparisler.Items.Count; i++)
                    {
                        saveOrder._MasaId = tableId;
                        saveOrder._UrunId = Convert.ToInt32(lvSiparisler.Items[i].SubItems[2].Text);
                        saveOrder._AdisyonId = newAdisyon.GetByAddition(tableId);
                        saveOrder._Adet = Convert.ToInt32(lvSiparisler.Items[i].SubItems[1].Text);
                        saveOrder.SetSaveOrder(saveOrder);
                    }
                    this.Close();
                    frmMasa.Show();
                }
            }
        }


        private void lvSiparisler_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (lvSiparisler.Items.Count > 0)
            {
                if (lvSiparisler.SelectedItems[0].SubItems[4].Text != "0")
                {
                    cSiparisler saveOrder = new cSiparisler();
                    saveOrder.SetDeleteOrder(Convert.ToInt32(lvSiparisler.SelectedItems[0].SubItems[4].Text));

                }
                else
                {
                    for (int i = 0; i < lvYeniEklenenler.Items.Count; i++)
                    {
                        if (lvYeniEklenenler.Items[i].SubItems[4].Text == lvSiparisler.SelectedItems[0].SubItems[5].Text)
                        {
                            lvYeniEklenenler.Items.RemoveAt(i);
                        }

                    }
                }
                lvSiparisler.Items.RemoveAt(lvSiparisler.SelectedItems[0].Index);
            }
        }

        private void txtAra_TextChanged(object sender, EventArgs e)
        {
            if (txtAra.Text == "")
            {
                txtAra.Text = "";
            }
            else
            {
                cUrunCesitleri cU = new cUrunCesitleri();
                cU.GetByProductForSearch(lvMenu, Convert.ToInt32(txtAra.Text));
            }

        }

        private void btnIptal_Click(object sender, EventArgs e)
        {
            frmMasalar frmMasa = new frmMasalar();
            this.Close();
            frmMasa.Show();
        }
        private void btnÖdeme_Click(object sender, EventArgs e)
        {
            cGenel._servisTurNo = 1;
            cGenel._adisyonId = additionId.ToString();
            frmBill frmOdeme = new frmBill();
            this.Close();
            frmOdeme.Show();
        }
        #endregion


    }
}

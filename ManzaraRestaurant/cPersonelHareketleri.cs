﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManzaraRestaurant
{
    class cPersonelHareketleri
    {
        cGenel genel = new cGenel();
        #region Property
        public int _ID { get; set; }
        public int _PersonelId { get; set; }
        public string Islem { get; set; }
        public DateTime _Tarih { get; set; }
        public bool Durum { get; set; }
        #endregion

        #region PersonelActionSave
        public bool PersonelActionSave(cPersonelHareketleri cPH)
        {
            bool result = false;
            SqlConnection con = new SqlConnection(genel.connectionString);
            SqlCommand cmd = new SqlCommand("AddPersonelHareketleri", con);
            cmd.CommandType = CommandType.StoredProcedure;
            

            try
            {
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }
                cmd.Parameters.AddWithValue("@P_PersonelId", cPH._PersonelId);
                cmd.Parameters.AddWithValue("@P_Islem", cPH.Islem);
                cmd.Parameters.AddWithValue("@P_Tarih", cPH._Tarih);

                result = Convert.ToBoolean(cmd.ExecuteNonQuery());

            }
            catch (SqlException ex)
            {
                string hata = ex.Message;

                throw;
            }
            return result;
        }

        #endregion

        #region GetPersonelHareketleri
        public DataTable GetPersonelHareketleri()
        {
            
            SqlConnection con = new SqlConnection(genel.connectionString);
            SqlDataAdapter adapter = new SqlDataAdapter("GetPersonelHareketleri", con);
            DataTable dt = new DataTable();
            try
            {
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }
                
                adapter.Fill(dt);
                

            }
            catch (SqlException ex)
            {
                string hata = ex.Message;

                throw;
            }
            
            return dt;
        }

        #endregion
    }
}

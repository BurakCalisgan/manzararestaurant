﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data;
using System.Data.SqlClient;

namespace ManzaraRestaurant
{
    class cUrunCesitleri
    {
        cGenel genel = new cGenel();
        #region Property
        public int _UrunTurNo { get; set; }
        public string _KategoriAd { get; set; }
        public int _Aciklama { get; set; }
        #endregion

        #region GetByProductTypes
        public void GetByProductTypes(ListView Cesitler, Button btn)
        {
            Cesitler.Items.Clear();
            SqlConnection con = new SqlConnection(genel.connectionString);
            SqlCommand cmd = new SqlCommand("GetByProductTypes", con);
            cmd.CommandType = CommandType.StoredProcedure;
            string aa = btn.Name;
            int uzunluk = aa.Length;
            cmd.Parameters.AddWithValue("@P_KATEGORI_ID", aa.Substring(uzunluk-1,1));
            SqlDataReader dr = null;
            try
            {
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }
                dr = cmd.ExecuteReader();
                int i = 0;
                while (dr.Read())
                {
                    Cesitler.Items.Add(dr["URUNAD"].ToString());
                    Cesitler.Items[i].SubItems.Add(dr["FIYAT"].ToString());
                    Cesitler.Items[i].SubItems.Add(dr["ID"].ToString());

                    i++;
                }

            }
            catch (SqlException ex)
            {
                string hata = ex.Message;
                throw;
            }
            finally
            {
                dr.Close();
                con.Dispose();
                con.Close();

            }
        }
        #endregion

        #region GetByProductForSearch
        public void GetByProductForSearch(ListView Cesitler, int txt)
        {
            Cesitler.Items.Clear();
            SqlConnection con = new SqlConnection(genel.connectionString);
            SqlCommand cmd = new SqlCommand("GetByProductForSearch", con);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@P_URUN_ID",txt);
            SqlDataReader dr = null;

            try
            {
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }
                dr = cmd.ExecuteReader();
                int i = 0;
                while (dr.Read())
                {
                    Cesitler.Items.Add(dr["URUNAD"].ToString());
                    Cesitler.Items[i].SubItems.Add(dr["FIYAT"].ToString());
                    Cesitler.Items[i].SubItems.Add(dr["ID"].ToString());

                    i++;
                }

            }
            catch (SqlException ex)
            {
                string hata = ex.Message;
                throw;
            }
            finally
            {
                dr.Close();
                con.Dispose();
                con.Close();

            }
        }
        #endregion

    }
}

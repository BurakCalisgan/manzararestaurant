﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManzaraRestaurant
{
    class cMasalar
    {
        #region Property
        public int ID { get; set; }
        public int _Kapasite { get; set; }
        public int _ServisTuru { get; set; }
        public int _Durum { get; set; }
        public int _Onay { get; set; }
        #endregion

        cGenel genel = new cGenel();
        #region SessionSum
        public string SessionSum(int state,string masaId)
        {
            string dt = "";
            SqlConnection con = new SqlConnection(genel.connectionString);
            SqlCommand cmd = new SqlCommand("SessionSum", con);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataReader dr = null;
            cmd.Parameters.AddWithValue("@P_DURUM", state);
            cmd.Parameters.AddWithValue("@P_MASA_ID", masaId);
            try
            {
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    dt = Convert.ToDateTime(dr["TARIH"]).ToString();
                }

            }
            catch (Exception ex)
            {
                string hata = ex.Message;
                throw;
            }
            finally
            {
                dr.Close();
                con.Dispose();
                con.Close();
            }
            return dt;
        }

        #endregion

        #region TableGetbyNumber
        public int TableGetbyNumber(string TableValue)
        {
            string aa = TableValue;
            int length = aa.Length;

            return Convert.ToInt32(aa.Substring(length - 1, 1));
        }
        #endregion

        #region TableGetbyState
        public bool TableGetbyState(int buttonName, int state)
        {
            bool result = false;
            SqlConnection con = new SqlConnection(genel.connectionString);
            SqlCommand cmd = new SqlCommand("TableGetbyState", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@P_TABLE_ID", buttonName);
            cmd.Parameters.AddWithValue("@P_STATE", state);

            try
            {
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }
                result = Convert.ToBoolean(cmd.ExecuteScalar());

            }
            catch (SqlException ex)
            {
                string hata = ex.Message;
                throw;
            }
            finally
            {
                con.Dispose();
                con.Close();
            }
            return result;
        }

        #endregion

        #region SetUpdateTableState
        public void SetUpdateTableState(string buttonName, int durum)
        {
            SqlConnection con = new SqlConnection(genel.connectionString);
            SqlCommand cmd = new SqlCommand("SetUpdateTableState", con);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }
                string aa = buttonName;
                int uzunluk = aa.Length;
                buttonName = aa.Substring(uzunluk - 1, 1);

                cmd.Parameters.AddWithValue("@P_MASA_ID", buttonName);
                cmd.Parameters.AddWithValue("@P_DURUM", durum);
                cmd.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                string hata = ex.Message;
                throw;
            }
            finally
            {
                con.Dispose();
                con.Close();
            }
        }


        #endregion
    }
}

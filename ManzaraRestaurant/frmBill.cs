﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ManzaraRestaurant
{
    public partial class frmBill : Form
    {
        public frmBill()
        {
            InitializeComponent();
        }

        #region Button Geri Dön - Çıkış
        private void btnGeriDon_Click(object sender, EventArgs e)
        {
            frmMenu frmMenu = new frmMenu();
            this.Close();
            frmMenu.Show();
        }

        private void btnCikis_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Çıkmak İstediğinizden Emin Misiniz?", "Uyarı !", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                Application.Exit();
            }
        }
        #endregion

        cSiparisler cS = new cSiparisler();
        int odemeTurId;
        private void frmBill_Load(object sender, EventArgs e)
        {
            if (cGenel._servisTurNo == 1)
            {
                lblAdisyonId.Text = cGenel._adisyonId;
                cS.GetByOrder(lvUrunler, Convert.ToInt32(lblAdisyonId.Text));
                txtIndirimTutari.TextChanged += new EventHandler(txtIndirimTutari_TextChanged);

                if (lvUrunler.Items.Count > 0)
                {
                    decimal toplam = 0;
                    for (int i = 0; i < lvUrunler.Items.Count; i++)
                    {
                        toplam += Convert.ToDecimal(lvUrunler.Items[i].SubItems[3].Text);
                    }

                    lblToplamTutar.Text = string.Format("{0:0.000}", toplam);
                    lblOdenecek.Text = string.Format("{0:0.000}", toplam);
                    decimal KDV = Convert.ToDecimal(lblOdenecek.Text) * 18 / 100;
                    lblKDV.Text = string.Format("{0:0.000}", KDV);
                }

                gbIndirim.Visible = true;
                txtIndirimTutari.Clear();
            }
            else if (cGenel._servisTurNo == 2)
            {
                lblAdisyonId.Text = cGenel._adisyonId;
                cPaketler cP = new cPaketler();
                odemeTurId = cP.GetPaymentType(Convert.ToInt32(lblAdisyonId.Text));
                cS.GetByOrder(lvUrunler, Convert.ToInt32(lblAdisyonId.Text));
                txtIndirimTutari.TextChanged += new EventHandler(txtIndirimTutari_TextChanged);

                if (odemeTurId == 1)
                {
                    rbNakit.Checked = true;
                }
                else if (odemeTurId == 2)
                {
                    rbKrediKartı.Checked = true;
                }
                else if (odemeTurId == 3)
                {
                    rbTicket.Checked = true;
                }



                if (lvUrunler.Items.Count > 0)
                {
                    decimal toplam = 0;
                    for (int i = 0; i < lvUrunler.Items.Count; i++)
                    {
                        toplam += Convert.ToDecimal(lvUrunler.Items[i].SubItems[3].Text);
                    }

                    lblToplamTutar.Text = string.Format("{0:0.000}", toplam);
                    lblOdenecek.Text = string.Format("{0:0.000}", toplam);
                    decimal KDV = Convert.ToDecimal(lblOdenecek.Text) * 18 / 100;
                    lblKDV.Text = string.Format("{0:0.000}", KDV);
                }

                gbIndirim.Visible = true;
                txtIndirimTutari.Clear();
            }

        }

        private void txtIndirimTutari_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToDecimal(txtIndirimTutari.Text) < Convert.ToDecimal(lblToplamTutar.Text))
                {
                    try
                    {
                        lblIndirim.Text = string.Format("{0:0.000}", Convert.ToDecimal(txtIndirimTutari.Text));
                    }
                    catch (Exception)
                    {
                        lblIndirim.Text = string.Format("{0:0.000}", 0);
                    }
                }
                else
                {
                    MessageBox.Show("İndirim Tutarı Toplam Tutardan Fazla Olamaz !");
                }
            }
            catch (Exception)
            {

                lblIndirim.Text = string.Format("{0:0.000}", 0);
            }
        }

        private void chkIndirim_CheckedChanged(object sender, EventArgs e)
        {
            if (chkIndirim.Checked)
            {
                gbAktivite.Visible = true;
                txtIndirimTutari.Clear();
            }
            else
            {
                gbAktivite.Visible = false;
                txtIndirimTutari.Clear();
            }
        }

        private void lblIndirim_TextChanged(object sender, EventArgs e)
        {
            if (Convert.ToDecimal(lblIndirim.Text) > 0)
            {
                decimal odenecek = 0;
                lblOdenecek.Text = lblToplamTutar.Text;
                odenecek = Convert.ToDecimal(lblOdenecek.Text) - Convert.ToDecimal(lblIndirim.Text);
                lblOdenecek.Text = string.Format("{0:0.000}", odenecek);

            }

            decimal KDV = Convert.ToDecimal(lblOdenecek.Text) * 18 / 100;
            lblKDV.Text = string.Format("{0:0.000}", KDV);
        }

        cMasalar cM = new cMasalar();
        cRezervasyon cR = new cRezervasyon();
        private void btnHesapKapat_Click(object sender, EventArgs e)
        {
            if (cGenel._servisTurNo == 1)
            {
                int masaId = cM.TableGetbyNumber(cGenel._buttonName);
                int musteriId = 0;
                if (cM.TableGetbyState(masaId, 4) == true)
                {
                    musteriId = cR.GetByClientIdFromRezervasyon(masaId);
                }
                else
                {
                    musteriId = 1;
                }

                int odemeTurId = 0;
                if (rbNakit.Checked)
                {
                    odemeTurId = 1;
                }
                if (rbKrediKartı.Checked)
                {
                    odemeTurId = 2;
                }
                if (rbTicket.Checked)
                {
                    odemeTurId = 3;
                }
                if (odemeTurId != 0)
                {
                    cOdeme odeme = new cOdeme();
                    odeme._AdisyonId = Convert.ToInt32(lblAdisyonId.Text);
                    odeme._OdemeTurId = odemeTurId;
                    odeme._MusteriId = musteriId;
                    odeme._AraToplam = Convert.ToDecimal(lblOdenecek.Text);
                    odeme._KdvTutari = Convert.ToDecimal(lblKDV.Text);
                    odeme._GenelToplam = Convert.ToDecimal(lblToplamTutar.Text);
                    odeme._Indirim = Convert.ToDecimal(lblIndirim.Text);

                    bool result = odeme.BillClose(odeme);

                    if (result == true)
                    {
                        MessageBox.Show("Hesap Kapatılmıştır.");
                        cM.SetUpdateTableState(Convert.ToString(masaId), 1);

                        cR.RezervationClose(Convert.ToInt32(lblAdisyonId.Text));

                        cAdisyon cA = new cAdisyon();
                        cA.AdditionClose(Convert.ToInt32(lblAdisyonId.Text), 0);

                        this.Close();
                        frmMasalar frmMasa = new frmMasalar();
                        frmMasa.Show();
                    }
                    else
                    {
                        MessageBox.Show("Hesap Kapatılırken Bir Hata Oluştu. Lütfen Yetkili Haber Veriniz !");
                    }
                }
                else
                {
                    MessageBox.Show("Lütfen Ödeme Türü Seçiniz !");
                }

            }//Paket Sipariş
            else if (cGenel._servisTurNo == 2)
            {
                cOdeme odeme = new cOdeme();
                odeme._AdisyonId = Convert.ToInt32(lblAdisyonId.Text);
                odeme._OdemeTurId = odemeTurId;
                odeme._MusteriId = 1; //Paket Sipariş olacak şekilde Id gelecek ;
                odeme._AraToplam = Convert.ToDecimal(lblOdenecek.Text);
                odeme._KdvTutari = Convert.ToDecimal(lblKDV.Text);
                odeme._GenelToplam = Convert.ToDecimal(lblToplamTutar.Text);
                odeme._Indirim = Convert.ToDecimal(lblIndirim.Text);

                bool result = odeme.BillClose(odeme);

                if (result == true)
                {
                    MessageBox.Show("Hesap Kapatılmıştır.");

                    cAdisyon cA = new cAdisyon();
                    cA.AdditionClose(Convert.ToInt32(lblAdisyonId.Text), 0);

                    cPaketler cP = new cPaketler();
                    cP.ClosePaketServis(Convert.ToInt32(lblAdisyonId.Text));



                    this.Close();
                    frmMasalar frmMasa = new frmMasalar();
                    frmMasa.Show();
                }
                else
                {
                    MessageBox.Show("Hesap Kapatılırken Bir Hata Oluştu. Lütfen Yetkili Haber Veriniz !");
                }
            }
        }

        private void btnHesapOzet_Click(object sender, EventArgs e)
        {
            printPreviewDialog1.ShowDialog();
        }

        Font baslik = new Font("Verdana", 20, FontStyle.Bold);
        Font altBaslik = new Font("Verdana", 15, FontStyle.Regular);
        Font icerik = new Font("Verdana", 13);
        SolidBrush sb = new SolidBrush(Color.Black);
        private void printDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            StringFormat st = new StringFormat();
            st.Alignment = StringAlignment.Near;
            e.Graphics.DrawString("Manzara Restaurant", baslik, sb, 270, 100, st);

            e.Graphics.DrawString("-----------------", altBaslik, sb, 350, 120, st);
            e.Graphics.DrawString("Ürün Adı                     Adet            Fiyat", altBaslik, sb, 150, 250, st);
            e.Graphics.DrawString("-------------------------------------------------------", altBaslik, sb, 150, 280, st);

            for (int i = 0; i < lvUrunler.Items.Count; i++)
            {
                e.Graphics.DrawString(lvUrunler.Items[i].SubItems[0].Text, icerik, sb, 150, 300 + i * 30, st);
                e.Graphics.DrawString(lvUrunler.Items[i].SubItems[1].Text, icerik, sb, 400, 300 + i * 30, st);
                e.Graphics.DrawString(lvUrunler.Items[i].SubItems[3].Text, icerik, sb, 530, 300 + i * 30, st);
            }
            e.Graphics.DrawString("-------------------------------------------------------", altBaslik, sb, 150, 300 + 30 * lvUrunler.Items.Count, st);
            e.Graphics.DrawString("İndirim Tutarı     : -------------" + lblIndirim.Text + " TL ", altBaslik, sb, 300, 300 + 30 * (lvUrunler.Items.Count + 1), st);
            e.Graphics.DrawString("KDV Tutarı          : -------------" + lblKDV.Text + " TL ", altBaslik, sb, 300, 300 + 30 * (lvUrunler.Items.Count + 2), st);
            e.Graphics.DrawString("Toplam Tutar      : -------------" + lblToplamTutar.Text + " TL ", altBaslik, sb, 300, 300 + 30 * (lvUrunler.Items.Count + 3), st);
            e.Graphics.DrawString("Ödediğiniz Tutar  : -------------" + lblOdenecek.Text + " TL ", altBaslik, sb, 300, 300 + 30 * (lvUrunler.Items.Count + 4), st);

        }
    }
}

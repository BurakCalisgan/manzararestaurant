﻿namespace ManzaraRestaurant
{
    partial class frmMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnMutfak = new System.Windows.Forms.Button();
            this.btnKasaIslemleri = new System.Windows.Forms.Button();
            this.btnCikis = new System.Windows.Forms.Button();
            this.btnKilit = new System.Windows.Forms.Button();
            this.btnAyarlar = new System.Windows.Forms.Button();
            this.btnRaporlar = new System.Windows.Forms.Button();
            this.btnMusteriler = new System.Windows.Forms.Button();
            this.btnPaketServis = new System.Windows.Forms.Button();
            this.btnRezervasyon = new System.Windows.Forms.Button();
            this.btnMasaSiparis = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnMutfak
            // 
            this.btnMutfak.BackgroundImage = global::ManzaraRestaurant.Properties.Resources.mutfak;
            this.btnMutfak.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnMutfak.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnMutfak.Location = new System.Drawing.Point(401, 224);
            this.btnMutfak.Name = "btnMutfak";
            this.btnMutfak.Size = new System.Drawing.Size(205, 137);
            this.btnMutfak.TabIndex = 1;
            this.btnMutfak.UseVisualStyleBackColor = true;
            this.btnMutfak.Click += new System.EventHandler(this.btnMutfak_Click);
            // 
            // btnKasaIslemleri
            // 
            this.btnKasaIslemleri.BackgroundImage = global::ManzaraRestaurant.Properties.Resources.kasaIslem;
            this.btnKasaIslemleri.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnKasaIslemleri.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnKasaIslemleri.Location = new System.Drawing.Point(256, 223);
            this.btnKasaIslemleri.Name = "btnKasaIslemleri";
            this.btnKasaIslemleri.Size = new System.Drawing.Size(140, 137);
            this.btnKasaIslemleri.TabIndex = 1;
            this.btnKasaIslemleri.UseVisualStyleBackColor = true;
            this.btnKasaIslemleri.Click += new System.EventHandler(this.btnKasaIslemleri_Click);
            // 
            // btnCikis
            // 
            this.btnCikis.BackgroundImage = global::ManzaraRestaurant.Properties.Resources.cikis1;
            this.btnCikis.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnCikis.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnCikis.Location = new System.Drawing.Point(497, 367);
            this.btnCikis.Name = "btnCikis";
            this.btnCikis.Size = new System.Drawing.Size(109, 115);
            this.btnCikis.TabIndex = 1;
            this.btnCikis.UseVisualStyleBackColor = true;
            this.btnCikis.Click += new System.EventHandler(this.btnCikis_Click);
            // 
            // btnKilit
            // 
            this.btnKilit.BackgroundImage = global::ManzaraRestaurant.Properties.Resources.kilitle;
            this.btnKilit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnKilit.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnKilit.Location = new System.Drawing.Point(369, 366);
            this.btnKilit.Name = "btnKilit";
            this.btnKilit.Size = new System.Drawing.Size(122, 116);
            this.btnKilit.TabIndex = 1;
            this.btnKilit.UseVisualStyleBackColor = true;
            this.btnKilit.Click += new System.EventHandler(this.btnKilit_Click);
            // 
            // btnAyarlar
            // 
            this.btnAyarlar.BackgroundImage = global::ManzaraRestaurant.Properties.Resources.ayarlar3;
            this.btnAyarlar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnAyarlar.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnAyarlar.Location = new System.Drawing.Point(241, 367);
            this.btnAyarlar.Name = "btnAyarlar";
            this.btnAyarlar.Size = new System.Drawing.Size(122, 115);
            this.btnAyarlar.TabIndex = 1;
            this.btnAyarlar.UseVisualStyleBackColor = true;
            this.btnAyarlar.Click += new System.EventHandler(this.btnAyarlar_Click);
            // 
            // btnRaporlar
            // 
            this.btnRaporlar.BackgroundImage = global::ManzaraRestaurant.Properties.Resources.rapor;
            this.btnRaporlar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnRaporlar.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnRaporlar.Location = new System.Drawing.Point(113, 367);
            this.btnRaporlar.Name = "btnRaporlar";
            this.btnRaporlar.Size = new System.Drawing.Size(122, 115);
            this.btnRaporlar.TabIndex = 1;
            this.btnRaporlar.UseVisualStyleBackColor = true;
            this.btnRaporlar.Click += new System.EventHandler(this.btnRaporlar_Click);
            // 
            // btnMusteriler
            // 
            this.btnMusteriler.BackgroundImage = global::ManzaraRestaurant.Properties.Resources.musteri;
            this.btnMusteriler.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnMusteriler.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnMusteriler.Location = new System.Drawing.Point(113, 223);
            this.btnMusteriler.Name = "btnMusteriler";
            this.btnMusteriler.Size = new System.Drawing.Size(137, 137);
            this.btnMusteriler.TabIndex = 1;
            this.btnMusteriler.UseVisualStyleBackColor = true;
            this.btnMusteriler.Click += new System.EventHandler(this.btnMusteriler_Click);
            // 
            // btnPaketServis
            // 
            this.btnPaketServis.BackgroundImage = global::ManzaraRestaurant.Properties.Resources.paketSiparis;
            this.btnPaketServis.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPaketServis.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnPaketServis.Location = new System.Drawing.Point(473, 80);
            this.btnPaketServis.Name = "btnPaketServis";
            this.btnPaketServis.Size = new System.Drawing.Size(133, 137);
            this.btnPaketServis.TabIndex = 1;
            this.btnPaketServis.UseVisualStyleBackColor = true;
            this.btnPaketServis.Click += new System.EventHandler(this.btnPaketServis_Click);
            // 
            // btnRezervasyon
            // 
            this.btnRezervasyon.BackgroundImage = global::ManzaraRestaurant.Properties.Resources.reserved;
            this.btnRezervasyon.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnRezervasyon.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnRezervasyon.Location = new System.Drawing.Point(327, 80);
            this.btnRezervasyon.Name = "btnRezervasyon";
            this.btnRezervasyon.Size = new System.Drawing.Size(140, 137);
            this.btnRezervasyon.TabIndex = 1;
            this.btnRezervasyon.UseVisualStyleBackColor = true;
            this.btnRezervasyon.Click += new System.EventHandler(this.btnRezervasyon_Click);
            // 
            // btnMasaSiparis
            // 
            this.btnMasaSiparis.BackgroundImage = global::ManzaraRestaurant.Properties.Resources.masa;
            this.btnMasaSiparis.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnMasaSiparis.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnMasaSiparis.Location = new System.Drawing.Point(113, 80);
            this.btnMasaSiparis.Name = "btnMasaSiparis";
            this.btnMasaSiparis.Size = new System.Drawing.Size(208, 137);
            this.btnMasaSiparis.TabIndex = 0;
            this.btnMasaSiparis.UseVisualStyleBackColor = true;
            this.btnMasaSiparis.Click += new System.EventHandler(this.btnMasaSiparis_Click);
            // 
            // frmMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Indigo;
            this.ClientSize = new System.Drawing.Size(721, 543);
            this.Controls.Add(this.btnMutfak);
            this.Controls.Add(this.btnKasaIslemleri);
            this.Controls.Add(this.btnCikis);
            this.Controls.Add(this.btnKilit);
            this.Controls.Add(this.btnAyarlar);
            this.Controls.Add(this.btnRaporlar);
            this.Controls.Add(this.btnMusteriler);
            this.Controls.Add(this.btnPaketServis);
            this.Controls.Add(this.btnRezervasyon);
            this.Controls.Add(this.btnMasaSiparis);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmMenu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmMenu";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnMasaSiparis;
        private System.Windows.Forms.Button btnRezervasyon;
        private System.Windows.Forms.Button btnPaketServis;
        private System.Windows.Forms.Button btnMusteriler;
        private System.Windows.Forms.Button btnKasaIslemleri;
        private System.Windows.Forms.Button btnMutfak;
        private System.Windows.Forms.Button btnRaporlar;
        private System.Windows.Forms.Button btnAyarlar;
        private System.Windows.Forms.Button btnKilit;
        private System.Windows.Forms.Button btnCikis;
    }
}
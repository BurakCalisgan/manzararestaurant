﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManzaraRestaurant
{
    class cPaketler
    {
        cGenel genel = new cGenel();
        #region Property
        public int _ID { get; set; }
        public int _AdisyonId { get; set; }
        public int _MusteriId { get; set; }
        public string _Aciklama { get; set; }
        public int _Durum { get; set; }
        public int _OdemeTurId { get; set; }
        #endregion


        #region AddPaketSiparis(PaketServisAçtık(Insert Yaptık))
        //Paket Servis açma işlemi
        public bool AddPaketSiparis(cPaketler cP)
        {
            bool result = false;
            SqlConnection con = new SqlConnection(genel.connectionString);
            SqlCommand cmd = new SqlCommand("AddPaketSiparis", con);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }
                cmd.Parameters.AddWithValue("@P_ADISYON_ID", cP._AdisyonId);
                cmd.Parameters.AddWithValue("@P_MUSTERI_ID", cP._MusteriId);
                cmd.Parameters.AddWithValue("@P_ODEME_TURU", cP._OdemeTurId);
                cmd.Parameters.AddWithValue("@P_ACIKLAMA", cP._Aciklama);
                result = Convert.ToBoolean(cmd.ExecuteNonQuery());
            }
            catch (SqlException ex)
            {
                string hata = ex.Message;
                throw;
            }
            finally
            {
                con.Dispose();
                con.Close();
            }

            return result;
        }

        #endregion

        #region DeletePaketServive(PaketServisinHesabınıKapatma(Durum Güncelleme))
        //Paket servisin durumunu güncelleme yani servisi kapatma işlemi
        public void ClosePaketServis(int adisyonId)
        {

            SqlConnection con = new SqlConnection(genel.connectionString);
            SqlCommand cmd = new SqlCommand("ClosePaketServis", con);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }
                cmd.Parameters.AddWithValue("@P_ADISYON_ID", adisyonId);
                Convert.ToBoolean(cmd.ExecuteNonQuery());
            }
            catch (SqlException ex)
            {
                string hata = ex.Message;
                throw;
            }
            finally
            {
                con.Dispose();
                con.Close();
            }

        }
        #endregion

        #region GetPaymentType
        //Açılan adisyon ve paket siparise ait ön girilen odeme tur ıd
        public int GetPaymentType(int adisyonId)
        {
            int odemeTuruId = 0;
            SqlConnection con = new SqlConnection(genel.connectionString);
            SqlCommand cmd = new SqlCommand("GetPaymentType", con);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }
                cmd.Parameters.AddWithValue("@P_ADISYON_ID", adisyonId);
                odemeTuruId = Convert.ToInt32(cmd.ExecuteScalar());
            }
            catch (SqlException ex)
            {
                string hata = ex.Message;
                throw;
            }
            finally
            {
                con.Dispose();
                con.Close();
            }
            return odemeTuruId;
        }

        #endregion

        #region MusteriSonAdisyonId
        //Sipariş Kontrol için müşteriye air açık olan en son adisyonId yi getirme
        //Bir müşterie ait 2 tane siparişin açık olamayacağını belirtiyoruz.
        public int GetMusteriSonAdisyonId(int musteriId)
        {
            int musteriNO = 0;
            SqlConnection con = new SqlConnection();
            SqlCommand cmd = new SqlCommand("GetMusteriSonAdisyonId", con);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }
                cmd.Parameters.AddWithValue("@P_MUSTERI_ID", musteriId);
                musteriNO = Convert.ToInt32(cmd.ExecuteScalar());
            }
            catch (SqlException ex)
            {
                string hata = ex.Message;
                throw;
            }
            finally
            {
                con.Dispose();
                con.Close();
            }
            return musteriNO;

        }
        #endregion

        #region GetCheckOpenAdisyonId
        //Müşteri arama ekranında adisyonbul butonu adisyon açık mı değil mi kontrol etmek için.
        public bool GetCheckOpenAdisyonId(int additionId)
        {
            bool result= false;
            SqlConnection con = new SqlConnection();
            SqlCommand cmd = new SqlCommand("GetCheckOpenAdisyonId", con);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }
                cmd.Parameters.AddWithValue("@P_ADISYON_ID", additionId);
                result = Convert.ToBoolean(cmd.ExecuteScalar());
            }
            catch (SqlException ex)
            {
                string hata = ex.Message;
                throw;
            }
            finally
            {
                con.Dispose();
                con.Close();
            }
            return result;

        }
        #endregion
    }
}
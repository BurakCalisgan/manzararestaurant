﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ManzaraRestaurant
{
    class cSiparisler
    {
        cGenel genel = new cGenel();
        #region Property
        public int ID { get; set; }
        public int _AdisyonId { get; set; }
        public int _UrunId { get; set; }
        public int _Adet { get; set; }
        public int _MasaId { get; set; }
        #endregion

        #region GetByOrder
        public void GetByOrder(ListView lv, int adisyonId)
        {
            SqlConnection con = new SqlConnection(genel.connectionString);
            SqlCommand cmd = new SqlCommand("GetByOrder", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@P_ADISYON_ID", adisyonId);
            SqlDataReader dr = null;
            try
            {
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }
                dr = cmd.ExecuteReader();
                int sayac = 0;
                while (dr.Read())
                {
                    lv.Items.Add(dr["URUNAD"].ToString());
                    lv.Items[sayac].SubItems.Add(dr["ADET"].ToString());
                    lv.Items[sayac].SubItems.Add(dr["URUNID"].ToString());
                    lv.Items[sayac].SubItems.Add(Convert.ToString(Convert.ToDecimal(dr["FIYAT"]) * Convert.ToDecimal(dr["ADET"])));
                    lv.Items[sayac].SubItems.Add(dr["ID"].ToString());
                    sayac++;
                }

            }
            catch (SqlException ex)
            {
                string hata = ex.Message;
                throw;
            }
            finally
            {
                dr.Close();
                con.Dispose();
                con.Close();

            }

        }
        #endregion

        #region SetSaveOrder
        public bool SetSaveOrder(cSiparisler Bilgiler)
        {
            bool result = false;
            SqlConnection con = new SqlConnection(genel.connectionString);
            SqlCommand cmd = new SqlCommand("SetSaveOrder", con);
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }
                cmd.Parameters.AddWithValue("@P_ADISYON_ID", Bilgiler._AdisyonId);
                cmd.Parameters.AddWithValue("@P_URUN_ID", Bilgiler._UrunId);
                cmd.Parameters.AddWithValue("@P_ADET", Bilgiler._Adet);
                cmd.Parameters.AddWithValue("@P_MASA_ID", Bilgiler._MasaId);
                result = Convert.ToBoolean(cmd.ExecuteNonQuery());

            }
            catch (SqlException ex)
            {
                string hata = ex.Message;
                throw;
            }
            finally
            {
                con.Dispose();
                con.Close();
            }
            return result;
        }

        #endregion

        #region SetDeleteOrder
        public void SetDeleteOrder(int satisId)
        {
            SqlConnection con = new SqlConnection(genel.connectionString);
            SqlCommand cmd = new SqlCommand("Satislar_Delete",con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@P_SATIS_ID", satisId);
            if (con.State==ConnectionState.Closed)
            {
                con.Open();
            }

            cmd.ExecuteNonQuery();
            con.Dispose();
            con.Close();
        }
        #endregion

    }
}

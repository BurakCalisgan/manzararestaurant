﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManzaraRestaurant
{
    class cRezervasyon
    {
        cGenel genel = new cGenel();
        #region Property
        public int _ID { get; set; }
        public int _TableId { get; set; }
        public int _MusteriId { get; set; }
        public DateTime _Date { get; set; }
        public int _MusteriSayisi { get; set; }
        public string _Aciklama { get; set; }
        public int _AdisyonId { get; set; }

        #endregion

        #region GetByClientIdFromRezervasyon
        //Rezervasyonlu müşteri için bilgileri getireceğiz.
        public int GetByClientIdFromRezervasyon(int tableId)
        {
            int clientId = 0;
            SqlConnection con = new SqlConnection(genel.connectionString);
            SqlCommand cmd = new SqlCommand("GetByClientIdFromRezervasyon", con);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }
                cmd.Parameters.AddWithValue("@P_MASA_ID", tableId);
                clientId = Convert.ToInt32(cmd.ExecuteNonQuery());
            }
            catch (SqlException ex)
            {
                string hata = ex.Message;
                throw;
            }
            finally
            {
                con.Dispose();
                con.Close();
            }
            return clientId;
        }
        #endregion

        #region RezervationClose
        //Hesap kapatılınca rezervasyonuda kapat 
        public bool RezervationClose(int adisyonId)
        {
            bool result = false;
            SqlConnection con = new SqlConnection(genel.connectionString);
            SqlCommand cmd = new SqlCommand("RezervationClose", con);
            cmd.CommandType = CommandType.StoredProcedure;
            try
            {
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }

                cmd.Parameters.AddWithValue("@P_ADISYON_ID", adisyonId);

                result = Convert.ToBoolean(cmd.ExecuteScalar());
            }
            catch (SqlException ex)
            {
                string hata = ex.Message;
                throw;
            }
            finally
            {
                con.Dispose();
                con.Close();
            }
            return result;
        }
        #endregion
    }
}

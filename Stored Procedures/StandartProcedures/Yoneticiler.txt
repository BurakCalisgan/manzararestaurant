//SELECT
IF EXISTS(SELECT * FROM sysobjects WHERE name = 'Yoneticiler_Select')
	DROP PROC Yoneticiler_Select
GO

CREATE PROC Yoneticiler_Select
	@ID int
AS

SELECT	ID,
	YoneticiAciklamasi
FROM	Yoneticiler
WHERE 	ID = @ID

GO

//INSERT
IF EXISTS(SELECT * FROM sysobjects WHERE name = 'Yoneticiler_Insert')
	DROP PROC Yoneticiler_Insert
GO

CREATE PROC Yoneticiler_Insert
	@ID int,
	@YoneticiAciklamasi nvarchar(100)
AS

INSERT Yoneticiler(ID, YoneticiAciklamasi)
VALUES (@ID, @YoneticiAciklamasi)

GO

//UPDATE
IF EXISTS(SELECT * FROM sysobjects WHERE name = 'Yoneticiler_Update')
	DROP PROC Yoneticiler_Update
GO

CREATE PROC Yoneticiler_Update
	@ID int,
	@YoneticiAciklamasi nvarchar(100)
AS

UPDATE	Yoneticiler
SET	YoneticiAciklamasi = @YoneticiAciklamasi
WHERE 	ID = @ID

GO

//DELETE
IF EXISTS(SELECT * FROM sysobjects WHERE name = 'Yoneticiler_Delete')
	DROP PROC Yoneticiler_Delete
GO

CREATE PROC Yoneticiler_Delete
	@ID int
AS

DELETE	Yoneticiler
WHERE 	ID = @ID

GO

//SELECT ALL
IF EXISTS(SELECT * FROM sysobjects WHERE name = 'Yoneticiler_SelectAll')
	DROP PROC Yoneticiler_SelectAll
GO

CREATE PROC Yoneticiler_SelectAll
AS

SELECT	ID,
	YoneticiAciklamasi
FROM	Yoneticiler

GO

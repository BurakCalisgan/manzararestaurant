//GetByClientIdFromRezervasyon
CREATE PROCEDURE [dbo].[GetByClientIdFromRezervasyon]
	@P_MASA_ID int
AS
BEGIN
	SELECT TOP 1 MUSTERIID
	FROM Rezervasyonlar
	WHERE MASAID=@P_MASA_ID
	ORDER BY MUSTERIID DESC
END

//RezervationClose
CREATE PROCEDURE [dbo].[RezervationClose]
	@P_ADISYON_ID int
AS
BEGIN
	UPDATE Rezervasyonlar
	SET DURUM=0
	WHERE ADISYONID=@P_ADISYON_ID
END
